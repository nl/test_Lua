mymetatable = {
    key2 = "key2",
    key99 = "value99"
}
mytable = setmetatable({
    key1 = "value1",
    key99 = "value99"
}, {
    __index = mymetatable
})

mytable.newkey = "newkey"
print(mytable.newkey, mymetatable.newkey)

mytable.key1 = "key1"
print(mytable.key1, mymetatable.key1)

mytable.key2 = "key2"
print(mytable.key2, mymetatable.key2)

mytable.key99 = "key99"
print(mytable.key99, mymetatable.key99)
