-- https://www.runoob.com/manual/lua53doc/manual.html#6.4.2
-- https://blog.csdn.net/oYuLinZuo/article/details/103800991

print("===============================================")
print("字节符 b 打包解包 pack(“b”,str） unpack(“b”,str）")
print("===============================================")
local unpack = string.unpack
local pack = string.pack
local str1 = pack(">b",-128) --最小支持 -128
local str2 = pack("<b",127) --最大支持 127

--如果把 pack("b",127) 改为 pack("b",128)，就会出现下面的错误
--bad argument #2 to 'pack' (integer overflow)，意思是pack的第二个参数整型溢出了
print(unpack(">b", str1)) --输出-128  2 ，这个2表示下一个字节的位置
print(unpack("<b", str2)) --输出127  2 ，这个2表示下一个字节的位置

print("===============================================")
print("字节符 h 打包解包 pack(“h”,str） unpack(“h”,str）")
print("===============================================")
local str1 = pack("h",-32768) --最小支持 -32768
local str2 = pack("h",32767) --最大支持 32767
--如果改为 pack("H",-32769) 或者 pack("H",32768)，就会出现下面的错误
--bad argument #2 to 'pack' (integer overflow)，意思是pack的第二个参数溢出了
print(unpack("h", str1)) --输出-32768  3 ，这个3表示下一个short的位置，每个short占2字节
print(unpack("h", str2)) -- 32767  3 ，这个3表示下一个short的位置


print("===============================================")
print("字节符 s 打包解包 pack(“s”,str） unpack(“s”,str）")
print("===============================================")
--  " > " 代表大端  s2表示加上2字节的头部长度  表示unsigned short

local unpack = string.unpack
local pack = string.pack

-- s,可指定头部占用字节数，默认占8字节
local temp1 = pack("s",'a')
print(unpack('s', temp1))
--输出 a    10,这个10表示下一个字符的位置

local temp2 = pack("s",'abc')
print(unpack('s', temp2))
--输出 abc	12,这个12表示下一个字符的位置

local temp3 = pack("ss",'abc','efg')
print(unpack('s', temp3, 12))
--输出 efg	23,这个23表示下一个字符的位置

-- s 默认头部占8字节，s1表示头部占1字节、s2表示头部占2字节

local temp4 = pack("s2",'abc')
print(unpack('s2', temp4))
--输出 abc    6,这个6表示下一个字符的位置

local temp5 = pack("s2s2",'abc','efg')
print(unpack('s2', temp5, 6))
--输出 efg	11,这个11表示下一个字符的位置


local text2 = pack(">s2", "123456789012")
--  " > " 代表大端  s2表示加上2字节的头部长度  表示unsigned short 
print(unpack(">s2", text2))

local temp4 = pack("s1",'abc')
print(unpack('s1', temp4))



print("===============================================")
print("测试")

-- s[n]: 长度加内容的字符串，其长度编码为一个 n 字节（默认是个 size_t） 长的无符号整数。
-- " > " 代表大端  s2表示加上2字节的头部长度  表示unsigned short  unsigned short
-- 如果是固定两个字节对应一个字符，两个字节最大的范围是 0-65535

-- 一个字节是：

-- 8位，每一位可以是0或1
-- 可以代表 256 个不同值的东西
-- 两个字节是..两个字节。

-- 16 位
-- 可以表示 65536 个不同值的东西
print("===============================================")
local temp6 = ""
local digit = 3 --字节数
local param = 2^(8*digit)-1

print("当前表示"..digit.."字节，表示最大范围为："..param)
for i = 1, param, 1 do
    temp6 = temp6 .. "1"
end


local temp4 = pack(">s"..digit,temp6)
local _ , b = unpack('>s'..digit, temp4)
print("下一个字符的位置：",b)

print("1~~~~~~~~~~~~~~~~~~~~~~~~~")
print(string.len("1"))
print(string.len(temp6))
print("2~~~~~~~~~~~~~~~~~~~~~~~~~")

