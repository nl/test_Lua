﻿local tab = {
    a1 = "a",
    a2 = "b1",
    a3 = "b2",
    a4 = "c"
}

local tabArr123 = {
    [1] = "a",
    [2] = "b",
    [4] = "d"
}

local asss121 = {"Lua1", "Tutorial2", "Lua3", "Tutorial4", "Lua5", "Tutorial6", "Lua7", "Tutorial8", "Lua9",
                 "Tutorial10", "Lua11", "Tutorial12"}

local asss10 = {
    [1] = "a1",
    [2] = "a2",
    [3] = "a3",
    [4] = "a4",
    [5] = "a5",
    [6] = "a6",
    [7] = "a7",
    [8] = "a8",
    [9] = "a9"
}

print("")
print("------------------------------------------------------------")
print("pairs 乱序输出")
print("")

print("-----------------------------")

for i, v in pairs(tab) do -- 输出 "a" ,"b", "c"  ,
    print(i, v)
end

print("-----------------------------")

for i, v in pairs(asss121) do -- 输出 "a" ,"b", "c"  ,
    print(i, v)
end

print("-----------------------------")

for i, v in pairs(asss10) do -- 输出 "a" ,"b", "c"  ,
    print(i, v)
end

print("")
print("------------------------------------------------------------")
print(
    "ipairs只能下标顺序为12345的 （例如234、则不行）。只能为顺序，中间中断。则不会继续进行遍历")
print("")

print("-----------------------------")

for i, v in ipairs(tab) do -- 输出 "a" ,k=2时断开 
    print(i, v)
end

print("-----------------------------")

for i, v in ipairs(tabArr123) do -- 输出 "a" ,k=2时断开 
    print(i, v)
end
