mymetatable = {
    key2 = "key2",
    key99 = "value99"
}
mytable = setmetatable({
    key1 = "value1",
    key99 = "value99"
}, {
    __newindex = mymetatable
})

mytable.newkey = "新值2"
print(mytable.newkey, mymetatable.newkey)

mytable.key1 = "新值1"
print(mytable.key1, mymetatable.key1)

mytable.key2 = "新值2"
print(mytable.key2, mymetatable.key2)

mytable.key99 = "新值99"
print(mytable.key99, mymetatable.key99)

-- 这个key两个表都没有，则付值给B表，A表内容不变，
-- 这个key - a表有的话，则付值给A表，B表内容不变，
-- 这个key - b表有的话，则付值给B表，A表内容不变，
-- 这个key两个表都有，	则付值给A表，B表内容不变，
