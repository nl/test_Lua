﻿function test_and()
    print(
        ".对于运算符and来说，如果它的第一个操作数为假，就返回第一个操作数；不然返回第二个操作数。")
    local a = 1
    local b = 1
    local c = 1

    a = b and c
    -- 等价于:

    if not b then
        a = b
    else
        a = c
    end

    print(2 and 3)
    print(0 and 3)
    print(nil and 7)
    print(false and 7)

end

function test_or()
    print(
        "对于运算符or来说，如果它的第一个操作数为真，就返回第一个操作数，不然返回第二个操作数。")

    local a = 1
    local b = 1
    local c = 1

    a = b or c
    -- 等价于:

    if b then
        a = b
    else
        a = c
    end
    -- 举个栗子：

    print(4 or 5)
    print(0 or 5)
    print(nil or 8)
    print(false or 8)
end

test_and()
test_or()
