﻿function CreateStudent(ID, Name)
    local Obj = {
        id = ID,
        name = Name
    };
    function Obj:GetID()
        return self.id;
    end
    function Obj.GetName(self)
        return self.name;
    end

    function Obj:SetID(ID)
        self.id = ID;
    end
    Obj.SetName = function(self, Name)
        self.name = Name
    end
    return Obj;
end

s1 = CreateStudent(1, "andy");
print("s1'id=", s1:GetID(), "s1'name=", s1.GetName(s1))

s1:SetID(2);
s1.SetName(s1, "lili");
print("s1'id=", s1:GetID(), "s1'name=", s1:GetName())

--  1、定义的时候：Class:test()与 Class.test(self)是等价的，点号(.)要达到冒号(:)的效果要加一个self参数到第一个参数；
-- 2、调用的时候：object:test() 与object.test(object)等价，点号(.)要添加对象自身到第一个参数。

-- 那么说 ， 方法都用 : 来定义 。比较方便
