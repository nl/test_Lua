﻿-- 上面的代码中，我们定义了一个 Singleton 类，通过 new() 方法创建一个新的对象，并使用 metatable 将其指向自身，从而实现类的封装。同时，我们还定义了一个名为 instance 的变量，用于保存 Singleton 实例化后的对象。
-- 在 getInstance() 方法中，我们首先判断 instance 是否为空，如果为空，则调用 new() 方法创建一个新的 Singleton 对象，并将其赋值给 instance 变量。否则，直接返回 instance 变量。
-- 通过这种方式，我们可以保证在整个应用程序中，只存在一个 Singleton 对象。同时，我们还可以使用 Singleton:new() 方法创建多个 Singleton 对象，但它们都是独立的，不会影响到 Singleton:getInstance() 方法返回的对象。


-- 定义 Singleton 类
local Singleton = {}
-- 定义 new 方法创建 Singleton 对象
function Singleton:new()
    local obj = {}
    -- 设置 obj 的元表为 Singleton 类自身，从而实现类的封装
    setmetatable(obj, self)
    -- 新创建的 obj 对象会继承 Singleton 类的属性和方法
    self.__index = self
    return obj
end
-- 定义 instance 变量，用于保存 Singleton 实例化后的对象
local instance = nil
-- 定义 getInstance 方法，用于返回 Singleton 实例化后的对象
function Singleton:getInstance()
    -- 判断 instance 是否为空
    if instance == nil then
        -- 如果为空，则调用 new 方法创建一个新的 Singleton 对象，并将其赋值给 instance 变量
        instance = Singleton:new()
    end
    -- 返回 instance 变量
    return instance
end
-- 返回 Singleton 类
return Singleton
